Project Name: Online English tutoring Application 

We are planning to do an entrepreneurial capstone project.

ABC English (randomly picked name) Tutoring Company is a startup in the online education field. Its business goal is to low the cost of online English tutoring and increase learning efficiency. 

Three parts are involved in the online education business: Foreign teachers, students(parents), and platform.

The current online English tutoring business model is that platform or company recruit foreign teachers and students on both sides by heavy advertising and promotion in marketing. And then match both sides through consultants in the platform. It is very costly and low efficient. 

Our goal is to reduce the middle cost as most as possible by cut recruiting and promotion. In our new platform, the teacher's side and students' sides can match each other by themselves. Make the appointment, make the payment, then go to the online classroom (using the third party).

The hidden challenge in this project will be how to match them more accurately and quickly. We are considering implementing more feature of teachers and students, for instance, the location (some students prefer specific areas accent teachers), age, sex, and education background, etc. If possible, we can try to implement third-party applications, like social media data, or algorithms to make the match more accurate. 

1) Foreign English teachers can register and login in to display their profiles. 
2) Students can register and login in to search for English tutors. 
3) Match, make a payment, and go online classroom.

About the tech, we need to use HTML, CSS, Javascript, MVC, and MongoDB database to build a basic mobile application. Then adapting some advanced code to make the matching more accurate.


--1) Foreign English teachers can register and login in to display their profiles. 
2) Students can register and login in to search for English tutors. 
3) Match, make a payment, and go online classroom.

About the tech, we need to use HTML, CSS, Javascript, MVC, and MongoDB database to build a basic mobile application. Then adapting some advanced code to make the matching more accurate.



License This project is licensed under the Mazon License - see the below for details

Copyright (c) <2021> <copyright Liqiang Li>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

