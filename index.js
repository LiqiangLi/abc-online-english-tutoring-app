// ------------------ Fill the following details -----------------------------
// Student name:Liqiang Li
// Student email:	Lli3479@conestogac.on.ca


/****************************Dependencies****************************/
// import dependencies you will use
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
//Destructuring an object example in tests.js
const { check, validationResult, header } = require('express-validator');
const { json } = require('body-parser');
//get express session
const session = require('express-session');

/****************************Database****************************/
//MongoDB
// Takes two arguments path - Includes type of DB, ip with port and name of database
mongoose.connect('mongodb://localhost:27017/awesomeStore',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

/****************************Models****************************/
const User = mongoose.model('User', {
    userLogin: String,
    userPass: String
});

// set up the model for the order
const Order = mongoose.model('Order', {
    customerName: String,
    customerNumber: String,
    blizzards: Number,
    sundaes: Number,
    shakes: Number
});

/****************************Variables****************************/
var myApp = express();
myApp.use(express.urlencoded({ extended: false }));
// Setup session to work with app

myApp.use(session({
    secret: 'superrandomsecret',//Should look more like 4v2j3h4h4b324b24k2b3jk4b24kj32nb4
    resave: false,
    saveUninitialized: true
}));

// adding a middleware to prevent caching so that admin pages cannot be accessed using back button.
myApp.use(function (req, res, next) {
    res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
});

//parse application json
myApp.use(express.json());
// set path to public folders and view folders
myApp.set('views', path.join(__dirname, 'views'));
//use public folder for CSS etc.
myApp.use(express.static(__dirname + '/public'));
myApp.set('view engine', 'ejs');

/****************************Page Routes****************************/
//Delete page
//Use uniques mongodb id
myApp.get('/delete/:orderid', function (req, res) {
    // check if the user is logged in
    if (req.session.userLoggedIn) {
        //delete
        var orderid = req.params.orderid;
        console.log(orderid);
        Order.findByIdAndDelete({ _id: orderid }).exec(function (err, order) {
            console.log('Error: ' + err);
            console.log('Order: ' + order);
            if (order) {
                res.render('delete', { message: 'Successfully deleted!', userLoggedIn: req.session.userLoggedIn });
            }
            else {
                res.render('delete', { message: 'Sorry, could not delete!', userLoggedIn: req.session.userLoggedIn });
            }
        });
    }
    else {
        res.redirect('/login');
    }
});


//Login Page
myApp.get('/login', function (req, res) {
    res.render('login', { userLoggedIn: req.session.userLoggedIn });
});

//Write some validations in the post for both username and password

myApp.post('/login', function (req, res) {
    var user = req.body.username;
    var pass = req.body.password;
    User.findOne({ userLogin: user, userPass: pass }).exec(function (err, admin) {
        // log any errors
        console.log('Error: ' + err);
        console.log('Admin: ' + admin);
        if (admin) {
            //store username in session and set logged in true
            req.session.username = admin.username;
            req.session.userLoggedIn = true;
            // redirect to the dashboard
            res.redirect('/allorders');
        }
        else {
            res.render('login', { error: 'Sorry, cannot login!' });
        }

    });

});
//fetch all. If there is an error it will put it in the err variable otherwise the orders
//will be returned in the orders variable.
// All orders page
myApp.get('/allorders', function (req, res) {
    // check if the user is logged in
    console.log(req.session.username);
    if (req.session.userLoggedIn) {
        console.log(req.session.userLoggedIn);
        Order.find({}).exec(function (err, orders) {
            res.render('allorders', { orders: orders, userLoggedIn: req.session.userLoggedIn });
        });
    }
    else { // otherwise send the user to the login page
        res.redirect('/login');
    }
});

//Logout Page
myApp.get('/logout', function (req, res) {
    //Remove variables from session
    req.session.username = '';
    req.session.userLoggedIn = false;
    req.session.destroy;

    res.render('form', { error: "Logged Out" });
});

//home page
myApp.get('/', function (req, res) {
    res.render('form', { userLoggedIn: req.session.userLoggedIn }); // no need to add .ejs to the file name
});

myApp.post('/', [
    check('customerName', 'Name is required!').notEmpty(),
    check('customerNumber', 'Phone is required').notEmpty(),
], function (req, res) {
    const errors = validationResult(req);
    console.log(errors);//logging this error will show us in the terminal that errors is an array and msg is what we need to print client side
    if (!errors.isEmpty()) {
        res.render('form', {
            errors: errors.array()
        });
    }
    else {
        var customerName = req.body.customerName;
        var customerNumber = req.body.customerNumber;
        var blizzards = req.body.blizzards;
        var sundaes = req.body.sundaes;
        var shakes = req.body.shakes;

        var pageData = {
            customerName: customerName,
            customerNumber: customerNumber,
            blizzards: blizzards,
            sundaes: sundaes,
            shakes: shakes
        }

        var myNewOrder = new Order(
            pageData
        );
        myNewOrder.save().then(() => console.log('New order saved'));

        res.render('form', pageData);
    }
});

/****************************Validation Functions****************************/
// phone regex for 123-123-2341
var phoneRegex = /^[0-9]{3}\-?[0-9]{3}\-?[0-9]{4}$/;
// positive number regex
var positiveNumberRegex = /^[1-9][0-9]*$/

//Function to check a string using regex
function checkRegex(userInput, regex) {
    if (regex.test(userInput)) {
        return true;
    }
    return false;
}

function customTestValidation(value) {
    throw new Error('This is an error');
}

// Custom validation functions return true if conditions are satisfied or throws an error of type Error
// Custom phone validation
function customPhoneValidation(value) {
    console.log(json.toString(value));
    console.log(value);
    console.log(value[0]);
    console.log(value[1]);

    if (!checkRegex(value, phoneRegex)) {
        throw new Error('Phone should be in the format (xxx)-xxx-xxxx');
    }
    return true;
}

//---------- Do not modify anything below this other than the port ------------
//------------------------ Setup the database ---------------------------------

myApp.get('/setup',function(req, res){
    
    let userData = [{
        'userLogin': 'admin',
        'userPass': 'admin'
    }];
    
    User.collection.insertMany(userData);

    var firstNames = ['John ', 'Alana ', 'Jane ', 'Will ', 'Tom ', 'Leon ', 'Jack ', 'Kris ', 'Lenny ', 'Lucas '];
    var lastNames = ['May', 'Riley','Rees', 'Smith', 'Walker', 'Allen', 'Hill', 'Byrne', 'Murray', 'Perry'];

    let ordersData = [];

    for(i = 0; i < 10; i++){
        let tempMemb = Math.floor((Math.random() * 100)) + '-AB' + '-' + Math.floor((Math.random() * 1000))
        let tempName = firstNames[Math.floor((Math.random() * 10))] + lastNames[Math.floor((Math.random() * 10))];
        let tempOrder = {
            customerName: tempName,
            customerNumber: tempMemb,
            blizzards: Math.floor((Math.random() * 10)),
            sundaes: Math.floor((Math.random() * 10)),
            shakes: Math.floor((Math.random() * 10))
        };
        ordersData.push(tempOrder);
    }
    
    Order.collection.insertMany(ordersData);
    res.send('Database setup complete. You can now proceed with your exam.');
    
});

// start the server and listen at a port
myApp.listen(8080);

//tell everything was ok
console.log('Everything executed fine.. website at port 8080....');

